<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Vault</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="style/style.css" />
</head>

<script>
    var vaultCodePvlt = "121";
    var guessedCodePvlt = "";
    var buttonsPressedPvlt = 0;

    function buttonClick(buttonIDPvlt) 
    {
        buttonsPressedPvlt++;
        guessedCodePvlt = guessedCodePvlt + buttonIDPvlt;

        alert("Code is " + guessedCodePvlt);

        if (buttonsPressedPvlt == 3)
        {
            if (guessedCodePvlt == vaultCodePvlt)
            {
                alert("The code is correct! Unlocking.");
            }

            else
            {
                alert("The code is incorrect. Resetting.");
            }

            document.getElementById("ButtonOnePvlt").disabled = true;
            document.getElementById("ButtonTwoPvlt").disabled = true;
            document.getElementById("ButtonThreePvlt").disabled = true;

            buttonsPressedPvlt = 0;
            guessedCodePvlt = "";
            alert("Three buttons have been pressed. Resetting code.");
        }
    }

    
</script>

<body>
    <div id="MainDivPvlt">
        <div id="ColorDivPvlt">
            <div id="GreenDivPvlt"></div>
            <div id="RedDivPvlt"></div>
        </div>
        <div id="MainContentPvlt">
            <h1>Enter your code</h1>
            <div id="ButtonsDivPvlt">
                <button class="ButtonPvlt" id="ButtonOnePvlt" onclick="buttonClick(1)">1</button>
                <button class="ButtonPvlt" id="ButtonTwoPvlt" onclick="buttonClick(2)">2</button>
                <button class="ButtonPvlt" id="ButtonThreePvlt" onclick="buttonClick(3)">3</button>
            </div>
        </div>
    </div>
</body>

</html>